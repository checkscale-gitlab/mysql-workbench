# WHY EXISTS?

To be able to use Mysql Workbench graphical user interface without having to
permanently install it in our computer.

Upgrading the application will be easy as building a new Docker Image for the
new version without loosing the current version.

This approach as the advantage of avoiding bloating the operating system of our
computer with one more software application, while allowing us to use it as it
if it was installed by the conventional way.

At same time will make easier the future upgrades of the operating system once
we don't need to worry about compatibility issues or about the overall
installation process.

Moving to another Operating System or computer only requires to backup the docker
volumes used for persisting data and settings.

Improves security of our computer once the Docker Container will act as a sand
box environment that will isolate the application from the host running it.


---

[<< previous](https://gitlab.com/exadra37-docker/mysql/workbench/blob/master/README.md) | [next >>](https://gitlab.com/exadra37-docker/mysql/workbench/blob/master/docs/the-package/what_is_it.md)

[HOME](https://gitlab.com/exadra37-docker/mysql/workbench/blob/master/README.md)

